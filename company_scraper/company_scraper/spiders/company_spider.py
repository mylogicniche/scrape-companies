import scrapy
from lxml import etree

class CompanySpider(scrapy.Spider):
    name = "companies"

    start_urls = [
        'http://www.firmarehberi.com/sitemap.xml',
    ]

    def parse(self, response):
        root = etree.fromstring(response.text.encode("utf-8"))
        #allurls = set()
        for sitemap in root:
            url = sitemap.getchildren()[0].text
            if not "firmalari" in url:
                #allurls.add(url)
                yield scrapy.Request(url, callback=self.parse_com)

    def parse_com(self, response):
        selist = response.xpath('//table[contains(@class, "corp-info")]/tr')
        res = {}
        for sel in selist:
            key = sel.css("td::text").extract_first().strip().lower().replace(" ","")
            if sel.css("td")[2].css("a"):
                val = sel.css("td")[2].css("a::text").extract()
            else:
                val = sel.css("td::text")[2].extract()
            print({key: val})
            res.update({key: val})
        yield res
